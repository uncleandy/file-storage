package filer

import (
	"crypto/sha256"
	"fmt"
	"github.com/pkg/errors"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttprouter"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"io"
	"io/ioutil"
	"log"
	"mime/multipart"
	"os"
	"path/filepath"
)

func Upload(ctx *fasthttp.RequestCtx, _ fasthttprouter.Params) {
	fileTmpName, file, err := saveFileToTemp(ctx)
	if err != nil {
		uploadError(ctx, errors.Wrap(err, "Open"))
		return
	}

	f := &models.File{}

	f.Hash, err = calcHashFile(fileTmpName)
	if err != nil {
		uploadError(ctx, errors.Wrap(err, "Sha256"))
		return
	}

	f.Ext = filepath.Ext(file.Filename)
	f.ContentType = file.Header.Get(fasthttp.HeaderContentType)

	// Сохраняем информацию о файле в БД
	trx, err := f.InsertDB()
	if err != nil {
		if trx != nil { _ = trx.RollbackTransaction() }
		uploadError(ctx, errors.Wrap(err, "Db.Insert"))
		return
	}

	// Сохраняем файл в хранилище
	err = currentStorage.SaveFile(fileTmpName, f)
	if err != nil {
		_ = trx.RollbackTransaction()
		uploadError(ctx, errors.Wrap(err, "SaveFile"))
		return
	}

	err = trx.CommitTransaction()
	if err != nil {
		_ = trx.RollbackTransaction()
		uploadError(ctx, errors.Wrap(err, "Commit"))
		return
	}

	respStr := fmt.Sprintf(`{
		"status": "ok",
		"id": %d,
		"hashSha256": %s
	}`, f.ID, f.Hash)

	ctx.SuccessString("application/json", respStr)
}

func saveFileToTemp(ctx *fasthttp.RequestCtx) (string, *multipart.FileHeader, error) {
	file, err := ctx.FormFile("file")
	if err != nil {
		return "", nil, errors.Wrap(err, "FormFile")
	}

	fileTmp, err := ioutil.TempFile(tempDir, "tmp_")
	if err != nil {
		return "", nil, errors.Wrap(err, "TempFile")
	}
	fileTmpName := fileTmp.Name()
	_ = fileTmp.Close()

	err = fasthttp.SaveMultipartFile(file, fileTmpName)
	if err != nil {
		return "", nil, errors.Wrap(err, "SaveMultipartFile")
	}

	return fileTmpName, file, nil
}

func calcHashFile(filePath string) (string, error) {
	hashSha256 := sha256.New()
	inFile, err := os.Open(filePath)
	if err != nil {
		return "", errors.Wrap(err, "Open")
	}
	defer inFile.Close()
	if _, err := io.Copy(hashSha256, inFile); err != nil {
		return "", errors.Wrap(err, "Copy")
	}

	return fmt.Sprintf("%x", hashSha256.Sum(nil)), nil
}

func uploadError(ctx *fasthttp.RequestCtx, err error) {
	log.Printf("Upload file error: %s", err)
	ctx.SuccessString("application/json", `{
		"status": "error",
		"error": "Error upload file: `+err.Error()+`"
	}`)
}
