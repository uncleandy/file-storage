package adapter

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"gitlab.com/uncleandy/file-storage/services/filer/storages"
)

/*
	Модуль отвечает за конвертацию URI хранилища для файла в
	вывод ответа пользователю в HTTP
*/

type Adapter interface {
	// Формирует ответ пользователю при запросе определенного файла
	Response(ctx *fasthttp.RequestCtx, file *models.File, storage *storages.Storage)
}
