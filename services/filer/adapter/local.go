package adapter

import (
	"bufio"
	"github.com/valyala/fasthttp"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"gitlab.com/uncleandy/file-storage/services/filer/storages"
	"io"
	"log"
)

// Выдает пользователю файл напрямую
type LocalAdapter struct {
	rootPath string
}

func NewLocalAdapter(rootDir string) *LocalAdapter {
	return &LocalAdapter{
		rootPath: rootDir,
	}
}

func (adapter *LocalAdapter) Response(ctx *fasthttp.RequestCtx, file *models.File, fileStorage storages.Storage) {
	var err error

	// Учет смещения для докачки
	rangeHeader := ctx.Request.Header.Peek("Range")
	posStart := 0
	posEnd := int(file.Size)
	if string(rangeHeader) != "" {
		posStart, posEnd, err = fasthttp.ParseByteRange(rangeHeader, int(file.Size))
		if err != nil {
			log.Printf("File range detect error: %s", err)
		}
	}

	// Получаем файл для чтения
	f, err := fileStorage.OpenForRead(file)
	if err != nil {
		log.Printf("Open file error: %s", err)
		ctx.Error(err.Error(), fasthttp.StatusNotFound)
		return
	}
	defer f.Close()

	// Задаем позицию для чтения
	_, err = f.Seek(int64(posStart), io.SeekStart)
	if err != nil {
		log.Printf("Seek file error: %s", err)
		ctx.Error(err.Error(), fasthttp.StatusNotFound)
		return
	}

	// Отдаем файл пользователю
	err = ctx.Response.ReadLimitBody(bufio.NewReader(f), posEnd - posStart)
	if err != nil {
		log.Printf("Response read file error: %s", err)
		ctx.Error(err.Error(), fasthttp.StatusNotFound)
		return
	}
	ctx.Response.Header.Set(fasthttp.HeaderContentType, file.ContentType)
	ctx.Response.Header.Set(fasthttp.HeaderContentDisposition, `attachment; filename="`+file.Name+`"`)
	ctx.Response.Header.Set(fasthttp.HeaderAcceptRanges, "bytes")
}
