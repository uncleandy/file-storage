package adapter

import (
	"github.com/labstack/gommon/log"
	"github.com/valyala/fasthttp"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"gitlab.com/uncleandy/file-storage/services/filer/storages"
	"strings"
)

type InternalRedirectAdapter struct {
	intLocation string		// Внутренняя локация (например в nginx) для выдачи файлов
	rootPath	string		// Корневой каталог в локальной fs, соответствующий внутренней локации
							// (для вычисления относительного пути файля для URL)
}

func NewInternalRedirectAdapter(root, location string) *InternalRedirectAdapter {
	return &InternalRedirectAdapter{
		intLocation: location,
		rootPath: root,
	}
}

func (adapter *InternalRedirectAdapter) Response(ctx *fasthttp.RequestCtx, file *models.File, fileStorage storages.Storage) {
	fileUri := fileStorage.AccessURI(file)

	if !strings.HasPrefix(fileUri, adapter.rootPath) {
		log.Printf("File %s not from internal URL root %s", fileUri, adapter.rootPath)
		ctx.Error("File not found in storage", fasthttp.StatusNotFound)
		return
	}

	path := strings.TrimPrefix(fileUri, adapter.rootPath)
	if path[0] != '/' {
		path = "/" + path
	}

	redirectPath := adapter.intLocation + path
	if redirectPath[0] != '/' {
		redirectPath = "/" + redirectPath
	}

	ctx.Response.Header.Set("X-Accel-Redirect", redirectPath)
	ctx.SuccessString("text/plain", "")
}
