package adapter

import (
	"github.com/valyala/fasthttp"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"gitlab.com/uncleandy/file-storage/services/filer/storages"
	"log"
	"strings"
)

type ExternalRedirectAdapter struct {
	extBaseURL	string		// Базовый внешний URL для редиректа
	rootPrefix	string		// Префикс в file URI который нужно отбросить для формирования полного URL файла
}

func (adapter *ExternalRedirectAdapter) Response(ctx *fasthttp.RequestCtx, file *models.File, fileStorage storages.Storage) {
	fileUri := fileStorage.AccessURI(file)

	if !strings.HasPrefix(fileUri, adapter.rootPrefix) {
		log.Printf("File %s not from internal URL root %s", fileUri, adapter.rootPrefix)
		ctx.Error("File not found in storage", fasthttp.StatusNotFound)
		return
	}

	path := strings.TrimPrefix(fileUri, adapter.rootPrefix)
	if path[0] != '/' {
		path = "/" + path
	}

	redirectPath := adapter.extBaseURL + path

	ctx.Redirect(redirectPath, fasthttp.StatusFound)
}
