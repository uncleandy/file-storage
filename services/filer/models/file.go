package models

import (
	"github.com/pkg/errors"
	"gitlab.com/uncleandy/file-storage/libs/db"
	"gitlab.com/uncleandy/file-storage/services/gql"
	"time"
)

type File struct {
	gql.File
}

func (file *File) InsertDB() (*db.Transaction, error) {
	trx, err := DbConn.StartTransaction()
	if err != nil {	return nil, err }

	createdAt := time.Now().UTC()
	fileId, err := trx.Insert(`
		INSERT INTO file_list
		(hash, size, name, content_type, ext, created_at)
		VALUES
		($1, $2, $3, $4, $5, $6)
	`, file.Hash, file.Size, file.Name, file.ContentType, file.Ext, createdAt)

	if err != nil {	return trx, err	}

	file.CreatedAt = createdAt
	file.ID = fileId

	return trx, nil
}

func (file *File) LoadDB() error {
	// Определяем по какому идентификатору (Hash имеет приоритет)
	var whereStr string
	var whereParam interface{}
	if file.Hash != "" {
		whereStr = "hash = $1"
		whereParam = file.Hash
	} else {
		whereStr = "id = $1"
		whereParam = file.ID
	}

	rows, err := DbConn.Query(`
		SELECT
			id, hash, size, name, content_type, ext, created_at
		FROM
			file_list
		WHERE `+whereStr,
		whereParam,
	)
	if err != nil { return err }
	defer rows.Close()
	if rows.Next() {
		err = rows.Scan(
			&file.ID,
			&file.Hash,
			&file.Size,
			&file.Name,
			&file.ContentType,
			&file.Ext,
			&file.CreatedAt,
		)
		if err != nil { return err }
	} else {
		return errors.New("NOT FOUND")
	}

	return nil
}
