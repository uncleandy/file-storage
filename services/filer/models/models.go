package models

import (
	"gitlab.com/uncleandy/file-storage/libs/db"
	"log"
)

var (
	DbConn			*db.Connection
)

func Init() {
	DbConn := db.New()
	if DbConn == nil {
		log.Fatalln("Can not connect to database")
	} else {
		err := DbConn.Db.Ping()
		if err != nil {
			log.Fatalf("Can not connect to database: %s", err)
		}
	}
}

