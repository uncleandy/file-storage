package storages

import (
	"fmt"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"os"
)

type LocalStorage struct {
	options		*LocalStorageOprions
}

type LocalStorageOprions struct {
	rootPath	string
}

func NewLocalStorage(options *LocalStorageOprions) *LocalStorage {
	return &LocalStorage{
		options: options,
	}
}

// Выдает строку URI для доступа к файлу
func (storage *LocalStorage) AccessURI(file *models.File) string {
	return storage.options.rootPath+"/"+fmt.Sprintf("%d", file.ID)+"."+file.Ext
}

// Сохраняет файл
func (storage *LocalStorage) SaveFile(source string, file *models.File) error {
	target := storage.AccessURI(file)

	err := os.Rename(source, target)
	if err != nil {	return err }

	return nil
}

func (storage *LocalStorage) OpenForRead(file *models.File) (*os.File, error) {
	filePath := storage.AccessURI(file)
	return os.Open(filePath)
}
