package storages

func Factory(storageName string) Storage {
	switch storageName {
	case "local":
		return &LocalStorage{}
	}

	return nil
}
