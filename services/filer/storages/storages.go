package storages

import (
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"os"
)

type Storage interface {
	// Метод сохранения файла в хранилище
	SaveFile(source string, file *models.File) error
	// Выдает строку URI для доступа к файлу (конкретный вид зависит от реализации)
	AccessURI(file *models.File) string
	// Открытие файла для выдачи его пользователю
	OpenForRead(file *models.File) (*os.File, error)
}
