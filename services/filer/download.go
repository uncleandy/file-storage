package filer

import (
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttprouter"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"strconv"
)

func Download(ctx *fasthttp.RequestCtx, params fasthttprouter.Params) {
	idStr := params.ByName("idStr")

	if idStr == "" {
		ctx.Error("File idStr is empty", fasthttp.StatusBadRequest)
		return
	}

	file := &models.File{}
	if id, err := strconv.ParseInt(idStr, 10, 64); err == nil {
		// Выбираем по ID
		file.ID = id
	} else {
		// Выбираем по хэшу
		file.Hash = idStr
	}

	err := file.LoadDB()
	if err != nil {
		ctx.Error(err.Error(), fasthttp.StatusNotFound)
		return
	}

	uri := currentStorage.AccessURI(file)







}