package filer

import "gitlab.com/uncleandy/file-storage/services/filer/storages"

var (
	currentStorage storages.Storage
	tempDir        string
)

func Init(tempPath string, storageName string) {
	tempDir = tempPath
	currentStorage = storages.Factory(storageName)
}
