package main

import (
	"flag"
	"fmt"
	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttprouter"
	"gitlab.com/uncleandy/file-storage/libs/db"
	"gitlab.com/uncleandy/file-storage/services/filer"
	"gitlab.com/uncleandy/file-storage/services/filer/models"
	"log"
)

func main() {
	host := flag.String("host", "localhost", "host for listen http connection")
	port := flag.Int("port", 8080, "port for listen http connection")
	saveFileDir := *flag.String("dir", "/tmp", "directory for save files")

	// Конфиг БД
	dbHost := flag.String("db_host", "localhost", "host of database server")
	dbPort := flag.Int("db_port", 5432, "port of database server")
	dbName := flag.String("db_name", "", "database name")
	dbUser := flag.String("db_user", "", "database user")
	dbPass := flag.String("db_pass", "", "database password")
	flag.Parse()

	if dbName == nil || *dbName == "" || dbUser == nil || *dbUser == "" || dbPass == nil || *dbPass == "" {
		flag.PrintDefaults()
		log.Fatal("Absent required parameters")
	}

	db.Init(&db.Settings{
		Host: *dbHost,
		Port: *dbPort,
		DBName: *dbName,
		User: *dbUser,
		Password: *dbPass,
	})

	models.Init()
	filer.Init(saveFileDir, "local")

	router := fasthttprouter.New()
	router.POST("/upload", filer.Upload)
	router.GET("/:id", filer.Download)

	listenStr := fmt.Sprintf("%s:%d", *host, *port)
	log.Fatal(fasthttp.ListenAndServe(listenStr, router.Handler))
}
