package code

import (
	"context"
	"gitlab.com/uncleandy/file-storage/libs/db"
	"log"
	"strconv"

	"gitlab.com/uncleandy/file-storage/services/gql"
)

// THIS CODE IS A STARTING POINT ONLY. IT WILL NOT BE UPDATED WITH SCHEMA CHANGES.

type Resolver struct{
	db *db.Connection
}

func NewResolver() *Resolver {
	dbConn := db.New()
	if dbConn == nil {
		log.Fatalln("Can not connect to database")
	} else {
		err := dbConn.Db.Ping()
		if err != nil {
			log.Fatalf("Can not connect to database: %s", err)
		}
	}

	return &Resolver{
		db: dbConn,
	}
}

func (r *Resolver) File() FileResolver {
	return &fileResolver{r}
}
func (r *Resolver) Query() QueryResolver {
	return &queryResolver{r}
}

type fileResolver struct{ *Resolver }

func (r *fileResolver) ID(ctx context.Context, obj *gql.File) (string, error) {
	return strconv.FormatInt(obj.ID, 10), nil
}
func (r *fileResolver) CreatedAt(ctx context.Context, obj *gql.File) (string, error) {
	timestamp := obj.CreatedAt.Unix() * 1000
	return strconv.FormatInt(timestamp, 10), nil
}

type queryResolver struct{ *Resolver }

func (r *queryResolver) Files(ctx context.Context, limit *int, offset *int) ([]*gql.File, error) {
	var files []*gql.File

	rows, err := r.db.Query(`
		SELECT id, hash, name, size, content_type, ext, created_at
		FROM files
		LIMIT $1
		OFFSET $2
		`,
		limit, offset,
	)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var file gql.File

		err = rows.Scan(
			&file.ID,
			&file.Hash,
			&file.Name,
			&file.Size,
			&file.ContentType,
			&file.Ext,
			&file.CreatedAt,
		)

		if err != nil { return nil, err	}

		files = append(files, &file)
	}

	return files, nil
}
