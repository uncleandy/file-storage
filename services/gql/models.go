package gql

import (
	"errors"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"io"
	"strconv"
	"time"
)

// МОДЕЛИ

type File struct {
	ID        		int64  		`json:"id"`
	Hash	  		string		`json:"hash"`
	Size      		int    		`json:"size"`
	Name      		string 		`json:"name"`
	CreatedAt 		time.Time  	`json:"createdAt"`
	ContentType  	string 		`json:"contentType"`
	Ext       		string		`json:"ext"`
}

// ТИПЫ ПОЛЕЙ

func MarshalID(id int64) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		_, _ = io.WriteString(w, strconv.Quote(fmt.Sprintf("%d", id)))
	})
}

func UnmarshalID(v interface{}) (int64, error) {
	id, ok := v.(string)
	if !ok {
		return 0, fmt.Errorf("ids must be strings")
	}
	i, e := strconv.ParseInt(id, 10, 64)
	return i, e
}

func MarshalTimestamp(t time.Time) graphql.Marshaler {
	timestamp := t.Unix() * 1000

	return graphql.WriterFunc(func(w io.Writer) {
		_, _ = io.WriteString(w, strconv.FormatInt(timestamp, 10))
	})
}

func UnmarshalTimestamp(v interface{}) (time.Time, error) {
	if tmpStr, ok := v.(int); ok {
		return time.Unix(int64(tmpStr), 0), nil
	}
	return time.Time{}, errors.New("error when convert timestamp")
}
