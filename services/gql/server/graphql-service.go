package main

import (
	"flag"
	"fmt"
	"github.com/99designs/gqlgen/handler"
	"gitlab.com/uncleandy/file-storage/libs/db"
	gqlCode "gitlab.com/uncleandy/file-storage/services/gql/code"
	"log"
	"net/http"
)

const defaultPort = "8080"

func main() {
	host := flag.String("host", "localhost", "host for listen http connection")
	port := flag.Int("port", 8080, "port for listen http connection")

	// Конфиг БД
	dbHost := flag.String("db_host", "localhost", "host of database server")
	dbPort := flag.Int("db_port", 5432, "port of database server")
	dbName := flag.String("db_name", "", "database name")
	dbUser := flag.String("db_user", "", "database user")
	dbPass := flag.String("db_pass", "", "database password")
	flag.Parse()

	if dbName == nil || *dbName == "" || dbUser == nil || *dbUser == "" || dbPass == nil || *dbPass == "" {
		flag.PrintDefaults()
		log.Fatal("Absent required parameters")
	}

	db.Init(&db.Settings{
		Host: *dbHost,
		Port: *dbPort,
		DBName: *dbName,
		User: *dbUser,
		Password: *dbPass,
	})

	resolver := gqlCode.NewResolver()

	http.Handle("/", handler.Playground("GraphQL playground", "/query"))
	http.Handle("/query", handler.GraphQL(gqlCode.NewExecutableSchema(gqlCode.Config{Resolvers: resolver})))

	httpStr := fmt.Sprintf("%s:%d", *host, *port)
	log.Printf("connect to http://%s/ for GraphQL playground", httpStr)
	log.Fatal(http.ListenAndServe(httpStr, nil))
}
